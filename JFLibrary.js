/*
 Base function file;
 - All functions should be made with return (if no value true/false)
 - All functions must be universal
 - All functions must have input data and comments of input and output data
 */
var JFLib = {
    req: false,
    error: {},
    debug: [],
    init: function () {
        //nothing here
    },
    check: {
        /*
         * Checks if string is DOMelement attreibute
         */
        isAttribute: function (attr) {
            if (self.check.type(attr)) {
                switch (attr) {
                    case('class'):
                        return true;
                    case('data'):
                        return true;
                    case('id'):
                        return true;
                    case('type'):
                        return true;
                    case('src'):
                        return true;
                    case('title'):
                        return true;
                    case('checked'):
                        return true;
                    case('selected'):
                        return true;
                    case('operator'):
                        return true;
                    case('against'):
                        return true;
                    case('values'):
                        return true;
                    case('alt'):
                        return true;
                    case('empty'):
                        return true;
                    case('max'):
                        return true;
                    case('name'):
                        return true;
                    case('search_string'):
                        return true;
                    case('use_current_page_value'):
                        return true;
                    default:
                        return false;
                }
            }
            return false;
        },
        /*
         *
         * @param {type} json
         * @returns {Boolean}
         */
        json: function (json) {
            var isJson = true;
            try {
                var json = jQuery.parseJSON(json);
            } catch (err) {
                isJson = false;
            }
            return isJson;
        },
        valueInObject: function (object, value) {
            try {
                if (object.hasOwnProperty(value)) {
                    return true;
                } else {
                    return false;
                }
            } catch (error) {
                self.error[jQuery.now()] = 'valueInObject: invalid input arguments';
                return false;
            }
        },
        /*
         *
         * @param {type} json
         * @param {type} value
         * @returns {Boolean}
         */
        valueInJson: function (json, value, loop) {
            if (loop > 100) {
                self.error[jQuery.now()] = 'valueInJson: crossed 100 obejct check';
                return self.tmp = false;
            }
            if (!self.check.type(value) || !self.check.type(json, 'object')) {
                return false;
            }
            if (typeof loop === 'undefined') {
                self.tmp = {};
                loop = 0;
            } else {
                loop++;
            }
            if (self.check.valueInObject(json, value)) {
                return self.tmp = true;
            } else {
                jQuery.each(json, function (id, val) {
                    if (self.check.type(val, 'object')) {
                        self.tmp = self.check.valueInJson(val, value, loop);
                    } else {
                        delete json[val];
                    }
                    if (self.tmp) {
                        return true;
                    }
                })
            }
            //console.log ( self.tmp )
            if (loop === 0) {
                return self.tmp;
            }
        },
        /*
         *
         * @param {type} input
         * @param {type} expectedType
         * @returns {Boolean}
         */
        type: function (input, expectedType) {
            if (self.isEmpty(input)) {
                return false;
            }
            if (typeof expectedType === 'undefined')
                expectedType = 'string';
            if (typeof input !== 'undefined') {
                if (expectedType === 'json')
                    return self.check.json(input);
                if (expectedType === 'array')
                    return Object.prototype.toString.call(input) === "[object Array]";
                if (expectedType === 'object')
                    return Object.prototype.toString.call(input) === "[object Object]";
                if (typeof input === expectedType)
                    return true;
            }
            return false;
        }
    },
    remove: {
        /*		 * 
         * String: "type" - class,id,name,input
         * String: "name"
         * @param {type} json
         * @returns {Boolean|Object}
         */

        element: function (obj) {
            if (typeof obj.name === 'string') {
                switch (obj.type) {
                    case('class'):
                        jQuery('.' + obj.name).remove();
                        return true;
                    case('id'):
                        jQuery('id' + obj.name).remove();
                        return true;
                    case('name'):
                        jQuery('[name="' + obj.name + '"]').remove();
                        return true;
                    default:
                        return false;
                }
            } else {
                return false;
            }
        }
    },
    convert: {
        /*
         * 
         * @param {type} json
         * @returns {Boolean|Object}
         */
        jsonToObject: function (json, Object) {
            if (self.check.type(json)) {
                if (self.check.json(json)) {
                    Object = jQuery.parseJSON(json);
                    return Object;
                }
                return false;
            }
            if (self.check.type(json, 'json')) {
                Object = json;
                return Object;
            } else {
                return false;
            }
            return false;
        },
        /*
         * 
         * @param {type} json
         * @returns {Boolean|String}
         */
        jsonToString: function (json, string) {
            if (self.check.type(json, 'object')) {
                try {
                    string = JSON.stringify(json);
                    return string;
                } catch (error) {
                    dni.error['jsonToString'] = jQuery.now() + ' : ' + error;
                    return false;
                }
            } else if (self.check.type(json)) {
                return json;
            } else {
                return false;
            }
        },
        /*
         * 
         * @param {type} string
         * @returns {Boolean|self.tmp2}
         */
        stringToJson: function (string, json) {
            if (self.check.type(string)) {
                try {
                    string = decodeURI(string);
                    if (self.check.type(string)) {
                        string = unescape(string);
                    }
                    return json = JSON.parse(string);
                } catch (error) {
                    dni.error['stringToJson'] = jQuery.now() + ': ' + error;
                    return false;
                }
            }
            return false;
        }
    },
    create: {
        /* 
         * To create new test we need:
         * STRING type - deepEqual,ok,notDeepEqual,equal,notStrictEqual,throws,strictEqual
         * FUNCTION func - current function
         * ARRAY parameters - parameters for the function
         * ANY result - expected result
         * OPTIONAL comment - test
         * Sample:
         * wp.createTest(deepEqual,self.check.type,{'input':'casd','type':'string'},true);
         */
        test: function (type, func, parameters, result, comment) {
            jQuery.each(parameters, function (id, value) {
                switch (typeof value) {
                    case('string'):
                        self.tmp += id + ':' + value + ' ';
                        break;
                    case('object'):
                        jQuery.each(value, function (id2, value2) {
                            switch (typeof value2) {
                                case('string'):
                                    self.tmp += id2 + ':' + value2 + ', ';
                                    break;
                            }
                        });
                        break;
                    case('undefined'):
                        break;
                }
            });
            if (typeof comment === 'undefined')
                comment = 'Parameters: ' + self.tmp.toString() + 'expected: ' + result;
            test('Dynamic test:', function () {
                type(func(parameters), result, comment);
            });
        }
    },
    get: {
        /*
         * 
         * @param {type} Obj
         * @returns {Boolean}
         */
        firstObject: function (Obj) {
            if (self.check.type(Obj, 'object')) {
                for (self.tmp in Obj) {
                    return self.tmp.toString();
                }
            }
            return false;
        },
        /*
         *
         * @param {type} obj
         * @param {type} key
         * @returns {Boolean|self.tmp}
         */
        valueByKey: function (obj, key, loop) {
            if (loop > 100) {
                self.error[jQuery.now()] = 'valueInJson: crossed 100 obejct check';
                return self.tmp = false;
            }
            if (!self.check.type(key) || !self.check.type(obj, 'object')) {
                return false;
            }
            if (typeof loop === 'undefined') {
                delete self.tmp;
                loop = 0;
            } else {
                loop++;
            }
            jQuery.each(obj, function (id, val) {
                if (id === key) {
                    return self.tmp = val;
                } else if (self.check.type(val, 'object')) {
                    self.tmp = self.get.valueByKey(val, key, loop);
                }
                delete obj[val];
            });
            if (self.tmp) {
                return self.tmp;
            } else {
                return false;
            }
        },
        /*
         * 
         * @param {type} Obj
         * @returns {Boolean}
         */
        lastObject: function (Obj) {
            if (self.check.type(Obj, 'object')) {
                var lastElement;
                for (self.tmp in Obj) {
                    if (self.check.type(self.tmp, 'string')) {
                        lastElement = self.tmp.toString();
                    } else {
                        return false;
                    }
                }
                return lastElement;
            }
            return false;
        },
        /*
         * 
         * @param {type} obj
         * @returns {undefined}
         */
        jsonData: function (obj) {
            jQuery.ajax({
                // the URL for the request
                url: obj.url,
                // the data to send (will be converted to a query string)
                data: obj.data,
                // whether this is a POST or GET request
                type: obj.type,
                // the type of data we expect back
                dataType: obj.dataType,
                // code to run if the request succeeds;
                // the response is passed to the function
                success: function (json) {
                    return json;
                },
                // code to run if the request fails; the raw request and
                // status codes are passed to the function
                error: function (xhr, status) {
                    self.error['ajax']("Sorry, there was a error with ajax call! XHR: " + xhr + " Status: " + status);
                },
                // code to run regardless of success or failure
                complete: function (xhr, status) {
                    //something
                }
            });
        },
        element: {
            value: function (element, key) {
                if (self.isEmpty(key)) {
                    return false;
                } else {
                    if (self.isEmpty(element)) {
                        return false;
                    } else {
                        return jQuery(element).attr(key);
                    }
                }
                return false;
            }
        }

    },
    /*
     * 
     * @param {type} obj
     * @returns {Boolean}
     */
    isEmpty: function (obj) {
        if (obj == null)
            return true;
        if (obj.length && obj.length > 0)
            return false;
        if (obj.length === 0)
            return true;
        for (var key in obj) {
            if (self.hasOwnProperty.call(obj, key))
                return false;
        }
        return true;
    },
    /*
     * 
     * @param {type} baseItem
     * @param {type} overrideItem
     * @param {type} loop
     * @returns {self.tmp|Boolean}
     */
    merge: function (baseItem, overrideItem, theSameStructure, replace, loop, init) {
        if (typeof loop === 'undefined') {
            loop = 0;
            init = 0;
        } else {
            init++;
        }
        if (typeof theSameStructure === 'undefined') {
            theSameStructure = false;
        }
        if (typeof replace === 'undefined') {
            replace = true;
        }
        if (self.check.type(baseItem, 'string') && self.check.type(overrideItem, 'string')) {
        } else if (self.check.type(baseItem, 'object') && self.check.type(overrideItem, 'object')) {
            if (theSameStructure) {
                jQuery.each(baseItem, jQuery.proxy(function (id, value) {
                    loop++;
                    if (self.check.type(value, 'object') && self.get.valueByKey(overrideItem, id)) {
                        jQuery.extend(value, overrideItem[id])
                        baseItem[id] = self.merge(value, overrideItem[id], theSameStructure, replace, loop, init);
                    } else {
                        if (replace) {
                            baseItem[id] = typeof overrideItem[id] !== 'undefined' ? overrideItem[id] : baseItem[id];
                        } else {
                            baseItem[id] = typeof overrideItem[id] !== 'undefined' ? baseItem[id] + ' ' + overrideItem[id] : baseItem[id];
                        }
                    }
                }));
            } else {
                jQuery.each(baseItem, jQuery.proxy(function (id, value) {
                    loop++;
                    if (self.check.type(value, 'object') && self.get.valueByKey(overrideItem, id)) {
                        baseItem[id] = self.merge(value, overrideItem[id], theSameStructure, replace, loop, init);
                    } else {
                        if (replace) {
                            baseItem[id] = self.get.valueByKey(overrideItem, id) !== false ? self.get.valueByKey(overrideItem, id) : baseItem[id];
                        } else {
                            baseItem[id] = self.get.valueByKey(overrideItem, id) !== false ? baseItem[id] + ' ' + self.get.valueByKey(overrideItem, id) : baseItem[id];
                        }
                    }
                }));
            }
        }
        return baseItem;
    },
    /*
     * 
     * @returns {Boolean}
     */
    checkRequirements: function (value) {
        if (self.check.type(value, 'object'))
            this.req = true;
        return false;
    }
};
var self = JFLib;
JFLib.init();